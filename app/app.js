let victoireplayer1 = 0;
let victoireplayer2 = 0;
const match = 1;

let weapons = {
    warrior: weaponsWarrior,
    assassin: weaponsAssassin,
    hunter: weaponsHunter,
    mage: weaponsMage,
    warlock: weaponsWarlock,
}

let build = {warrior, assassin, hunter, mage, warlock}

let buildSet1 = prompt("Saisissez votre Classe (sans faire de faute) : warrior | assassin | hunter | mage | warlock")
let playerName1 = prompt("Saisissez un nom pour votre Personnage")
let weapon1 = prompt('Saisissez une arme entre : ' + weapons[buildSet1].reduce((carry, weapon, index) => carry + index + ' : '  + weapon.name + ' ', ''))

build[buildSet1].equipWeapon(weapons[buildSet1][weapon1])
build[buildSet1].name = playerName1

let buildSet2 = prompt("Saisissez votre Opposant (sans faire de faute) : warrior | assassin | hunter | mage | warlock")
let playerName2 = prompt("Saisissez un nom pour votre Personnage")
let weapon2 = prompt('Saisissez une arme entre : ' + weapons[buildSet2].reduce((carry, weapon, index) => carry + index + ' : '  + weapon.name + ' ', ''))

build[buildSet2].equipWeapon(weapons[buildSet2][weapon2])
build[buildSet2].name = playerName2

function atk() {
    let player1 = Object.create(build[buildSet1]);
    let player2 = Object.create(build[buildSet2]);
    while (player1.isAlive() && player2.isAlive()) {
        for (let currentRound = 1; player1.isAlive() && player2.isAlive(); currentRound++) {
            console.log(' ');
            console.log("-----------------Round " + currentRound + " -----------------");
            player1.attack(player2);
            player2.attack(player1);
        }
    }
    if (player1.isAlive()) {
        victoireplayer1++;
    }
    if (player2.isAlive()) {
        victoireplayer2++;
    }
}

for (i = 1; i <= match; i++) {
    console.log(' ');
    console.log("___________________________" + "Match " + i + "___________________________");
    console.log(' ');
    atk();
}
console.log(' ');
console.log('______ ' + victoireplayer1 + ' Victoire de ' + playerName1 + " le " + buildSet1 +  ' contre ' + victoireplayer2 + ' Victoire de ' + playerName2 + " le " + buildSet2 + ' sur ' + match + ' Match' + ' ______')



