let mage = {
    name: "Magicien",
    weapon: null,
    armor: null,
    stamina: 100,
    strength: 2,
    agility: 7,
    intellect: 8,
    paradeStat: null,
    critical: function () {
        return 1.5 * mage.agility
    },
    dodge: function () {
        let chanceDodge = (Math.floor((Math.random() * 30))) + this.agility
        if (chanceDodge > 25) {
            return true
        } else {
            return false
        }
    },
    parade: function () {
        let paradeWeapon = (Math.floor((Math.random() * 30))) + this.paradeStat
        if (paradeWeapon > 25) {
            return true
        } else {
            return false
        }
    },
    ultimateSpell: function () {

    },
    spell: [
        lame,
        poison,
        feinte,
    ],
    attack: function (target) {
        let randomAttack = Math.ceil((Math.random() * 8))
        let dodgeOrParade = 1

        if (dodgeOrParade === 1) {
            if (this.dodge()) {
                console.log(target.name + " esquive la totaliter des degat de " + this.name + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
            } else {
                if (randomAttack > 0 && randomAttack <= 5) {
                    let criticalAttack = Math.floor((Math.random() * 2))
                    if (criticalAttack === 1) {
                        let damage = this.strength + this.critical()
                        target.stamina -= damage
                        console.log(target.name + " n'a pas pu esquiver latk de " + this.name + " " + damage + " degat critique en moins pour "
                            + target.name + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina);
                    } else {
                        target.stamina -= this.strength
                        console.log(target.name + " n'a pas pu esquiver latk de " + this.name + " " + this.strength + " degat en moins pour "
                            + target.name + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina);
                    }
                }
                if (randomAttack === 6) {
                    let spell = this.spell[0]
                    let damageSpell = spell.damage
                    let dodgeSpell = Math.floor((Math.random() * 3))
                    if (dodgeSpell === 1) {
                        console.log(this.name + " lance " + this.spell[0].name + " mais " + target.name + " esquive l'atk" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else if (dodgeSpell === 2) {
                        let damageCrit = damageSpell + this.critical()
                        target.stamina -= damageCrit
                        console.log(this.name + " lance " + this.spell[0].name + " celui ci critique ! " + target.name + " subit " + damageCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else {
                        let damageNoCrit = damageSpell
                        target.stamina -= damageNoCrit
                        console.log(this.name + " lance " + this.spell[0].name + " " + target.name + " subit " + damageNoCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    }
                }

                if (randomAttack === 7) {
                    let spell = this.spell[1]
                    let damageSpell = spell.damage
                    let dodgeSpell = Math.floor((Math.random() * 3))
                    if (dodgeSpell === 1) {
                        console.log(this.name + " lance " + this.spell[1].name + " mais " + target.name + " esquive l'atk" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else if (dodgeSpell === 2) {
                        let damageCrit = damageSpell + this.critical()
                        target.stamina -= damageCrit
                        console.log(this.name + " lance " + this.spell[0].name + " celui ci critique ! " + target.name + " subit " + damageCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else {
                        let damageNoCrit = damageSpell
                        target.stamina -= damageNoCrit
                        console.log(this.name + " lance " + this.spell[1].name + " " + target.name + " subit " + damageNoCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    }
                }

                if (randomAttack === 8) {
                    let spell = this.spell[2]
                    let damageSpell = spell.damage
                    let dodgeSpell = Math.floor((Math.random() * 3))
                    if (dodgeSpell === 1) {
                        console.log(this.name + " lance " + this.spell[2].name + " mais " + target.name + " esquive l'atk" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else if (dodgeSpell === 2) {
                        let damageCrit = damageSpell + this.critical()
                        target.stamina -= damageCrit
                        console.log(this.name + " lance " + this.spell[0].name + " celui ci critique ! " + target.name + " subit " + damageCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else {
                        let damageNoCrit = damageSpell
                        target.stamina -= damageNoCrit
                        console.log(this.name + " lance " + this.spell[2].name + " " + target.name + " subit " + damageNoCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    }
                }
            }
        }

        if (dodgeOrParade === 2) {
            if (this.parade()) {
                let parade = Number(this.paradeStat / 2)
                if (randomAttack <= 3) {
                    let criticalAttack = Math.floor((Math.random() * 3))
                    if (criticalAttack === 1) {
                        let damageCritical = this.strength + this.critical() - parade
                        target.stamina -= damageCritical
                        console.log(target.name + " a reussi a parer " + parade + " des degat critique de " + this.name + ", l'attaque de " + this.name + " inflige seulement " + damageCritical + " de degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else if (criticalAttack === 2) {
                        let damage = this.strength
                        target.stamina -= (damage - parade)
                        console.log(target.name + " a reussi a parer " + parade + " des degat de " + this.name + ", l'attaque de " + this.name + " inflige seulement " + (damage - parade) + " de degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else {
                        target.stamina -= this.strength
                        console.log(target.name + " n'a pas reussi a parer atk de " + this.name + ", " + this.name + " inglige " + this.strength + " a " + target.name + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    }
                } else if (randomAttack === 4) {
                    let spell = this.spell[0]
                    let damageSpell = spell.damage
                    let criticalAttack = Math.floor((Math.random() * 3))
                    if (criticalAttack === 1) {
                        let damageParadeCrit = damageSpell + this.critical() - parade
                        target.stamina -= damageParadeCrit
                        console.log(this.name + " lance " + this.spell[0].name + " et celui ci critique ! Mais " + target.name + " part " + parade + " des degats " + target.name + " ne subit que " + damageParadeCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else if (criticalAttack === 2) {
                        let damageParade = damageSpell - parade
                        target.stamina -= damageParade
                        console.log(this.name + " lance " + this.spell[0].name + " Mais " + target.name + " part " + parade + " des degats " + target.name + " ne subit que " + damageParade + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else {
                        let damage = damageSpell
                        target.stamina -= damage
                        console.log(this.name + " lance " + this.spell[0].name + " " + target.name + " subit " + damage + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    }
                } else if (randomAttack === 5) {
                    let spell = this.spell[1]
                    let damageSpell = spell.damage
                    let criticalAttack = Math.floor((Math.random() * 3))
                    if (criticalAttack === 1) {
                        let damageParadeCrit = damageSpell + this.critical() - parade
                        target.stamina -= damageParadeCrit
                        console.log(this.name + " lance " + this.spell[1].name + " et celui ci critique ! Mais " + target.name + " part " + parade + " des degats " + target.name + " ne subit que " + damageParadeCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else if (criticalAttack === 2) {
                        let damageParade = damageSpell - parade
                        target.stamina -= damageParade
                        console.log(this.name + " lance " + this.spell[1].name + " Mais " + target.name + " part " + parade + " des degats " + target.name + " ne subit que " + damageParade + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else {
                        let damage = damageSpell
                        target.stamina -= damage
                        console.log(this.name + " lance " + this.spell[1].name + " " + target.name + " subit " + damage + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    }
                } else {
                    let spell = this.spell[2]
                    let damageSpell = spell.damage
                    let criticalAttack = Math.floor((Math.random() * 3))
                    if (criticalAttack === 1) {
                        let damageParadeCrit = damageSpell + this.critical() - parade
                        target.stamina -= damageParadeCrit
                        console.log(this.name + " lance " + this.spell[2].name + " et celui ci critique ! Mais " + target.name + " part " + parade + " des degats " + target.name + " ne subit que " + damageParadeCrit + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else if (criticalAttack === 2) {
                        let damageParade = damageSpell - parade
                        target.stamina -= damageParade
                        console.log(this.name + " lance " + this.spell[2].name + " Mais " + target.name + " part " + parade + " des degats " + target.name + " ne subit que " + damageParade + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    } else {
                        let damage = damageSpell
                        target.stamina -= damage
                        console.log(this.name + " lance " + this.spell[2].name + " " + target.name + " subit " + damage + " degat" + " | " + target.name + " : " + target.stamina + " | " + this.name + " : " + this.stamina)
                    }
                }
            }
        }

    },
    equipWeapon: function (weapon) {
        this.weapon = weapon;
        this.strength += weapon.damage;
        this.stamina += weapon.stamina;
        this.agility += weapon.agility;
        this.paradeStat += weapon.paradeStat;
    },
    equipArmor: function (armor) {
        this.armor = armor
        // armure equiper
    },
    isAlive: function () {
        if (this.stamina > 0) {
            return true;
        }
        return false;
    },
}