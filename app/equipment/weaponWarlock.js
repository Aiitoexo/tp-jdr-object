let deadMagicWand = {
    name: "Baton des Damnée",
    damage: 16,
    stamina: 18,
    agility: 0,
    paradeStat: 7,
}

let daggerAndSkull = {
    name: "Dague & Crane",
    damage: 8,
    stamina: 20,
    agility: 8,
    paradeStat: 2,
}

let speremagic = {
    name: "Boule de vision",
    damage: 20,
    stamina: 12,
    agility: 10,
    paradeStat: 0,
}

let weaponsWarlock = [
    deadMagicWand,
    daggerAndSkull,
    speremagic,
]

