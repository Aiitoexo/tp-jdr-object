let dualDagger = {
    name: "Double Dagues",
    damage: 6,
    stamina: 0,
    agility: 15,
    paradeStat: 5,
}

let dualSword = {
    name: "Double épées",
    damage: 8,
    stamina: 2,
    agility: 8,
    paradeStat: 7,
}

let warglaive = {
    name: "Warglaive",
    damage: 10,
    stamina: 8,
    agility: 3,
    paradeStat: 6,
}

let weaponsAssassin = [
    dualDagger,
    dualSword,
    warglaive,
]

