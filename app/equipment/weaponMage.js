let magicWand = {
    name: "Baton Magique",
    damage: 12,
    stamina: 15,
    agility: 5,
    paradeStat: 8,
}

let septreAndShield = {
    name: "Septre & Bouclier",
    damage: 10,
    stamina: 20,
    agility: 5,
    paradeStat: 12,
}

let magicBook = {
    name: "Livre Magique",
    damage: 16,
    stamina: 10,
    agility: 12,
    paradeStat: 0,
}

let weaponsMage = [
    magicWand,
    septreAndShield,
    magicBook,
]

