let longArc = {
    name: "Arc Long",
    damage: 9,
    stamina: 5,
    agility: 7,
    paradeStat: 5,
}

let shortArc = {
    name: "Arc Court",
    damage: 7,
    stamina: 0,
    agility: 12,
    paradeStat: 6,
}

let crossbow = {
    name: "Arbalete",
    damage: 12,
    stamina: 8,
    agility: 0,
    paradeStat: 0,
}

let weaponsHunter = [
    longArc,
    shortArc,
    crossbow,
]

