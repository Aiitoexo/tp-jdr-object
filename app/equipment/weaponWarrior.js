let swordAndShield = {
    name: "Epee & Bouclier",
    damage: 7,
    stamina: 20,
    agility: 9,
    paradeStat: 15,
}

let twoHanded = {
    name: "Katana",
    damage: 15,
    stamina: 2,
    agility: 0,
    paradeStat: 10,
}

let twinAxe = {
    name: "Double Hache de Guerre",
    damage: 10,
    stamina: 0,
    agility: 7,
    paradeStat: 7,
}

let weaponsWarrior = [
    swordAndShield,
    twoHanded,
    twinAxe,
]

